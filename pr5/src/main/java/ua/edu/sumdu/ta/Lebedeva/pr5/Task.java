package ua.edu.sumdu.ta.Lebedeva.pr5;

import com.google.common.base.Preconditions;

public class Task {
    private String title;
    private int time;
    private int start;
    private int end;
    private int repeated;
    private boolean isRepeated;
    private boolean active = false;

    /**
     * Constructor for a single task
     *
     * @param title task name
     * @param time  time task, which is also equal to the start value and end value of the notifications
     */
    public Task(String title, int time) throws IllegalArgumentException {
        Preconditions.checkNotNull(title, "Title is null");
        if (time < 0)
            throw new IllegalArgumentException();
        this.title = title;
        this.time = time;
        this.start = time;
        this.end = time;
    }

    /**
     * Constructor for repeated task
     *
     * @param title  task's name
     * @param start  start value of the notifications
     * @param end    end value of the notifications
     * @param repeat repeated time
     */
    public Task(String title, int start, int end, int repeat) throws IllegalArgumentException {
        Preconditions.checkNotNull(title, "Title is null");
        if ((end < start) || (start < 0) || (end < 0) || (repeat < 0))
            throw new IllegalArgumentException();
        this.title = title;
        this.time = start;
        Preconditions.checkArgument(end > start, "negative value: end > start");
        this.start = start;
        this.end = end;
        this.repeated = repeat;
    }

    /**
     * method for getting the task's title
     *
     * @return task's name
     */
    public String getTitle() {
        return title;
    }

    /**
     * method for getting the work time
     *
     * @return time of a single notification for a single task or start of notification for repeated task.
     */
    public int getTime() {
        return time;
    }

    /**
     * method for getting the start time
     *
     * @return start of notification for repeated task
     */
    public int getStartTime() {
        return start;
    }

    /**
     * method for getting the end time
     *
     * @return end of notification for repeated task
     */
    public int getEndTime() {
        return end;
    }

    /**
     * method for getting  0 value, when it is a single task.
     * And for getting interval time, when it is a repeated task.
     *
     * @return repeated interval for repeated task or 0
     */
    public int getRepeatInterval() {
        if (!isRepeated)
            return 0;
        else
            return repeated;
    }

    /**
     * method sets the task's title
     *
     * @param title task's name
     */
    public void setTitle(String title) {
        Preconditions.checkNotNull(title, "Title is null");
        this.title = title;
    }

    /**
     * method return the task activity value
     *
     * @return activity value
     */
    public boolean isActive() {
        return active;
    }

    /**
     * method sets the task activity value
     *
     * @param active task activity indicator
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * method sets start value, end value and time for a single task
     *
     * @param time time of task notification
     */
    public void setTime(int time) throws IllegalArgumentException {
        if (time < 0)
            throw new IllegalArgumentException();
        this.start = time;
        this.end = time;
        this.time = time;
        this.repeated = 0;
    }

    /**
     * method sets start value, end value, time and number of repetitions for a repeated task
     *
     * @param start  the start time of the notification
     * @param end    the end time of the notification
     * @param repeat repeated interval
     */
    public void setTime(int start, int end, int repeat) throws IllegalArgumentException {
        if ((start < 0) || (end < 0) || (end < start) || (repeat < 0))
            throw new IllegalArgumentException();
        this.time = start;
        this.start = start;
        this.end = end;
        this.repeated = repeat;
        Preconditions.checkArgument(end > start, "negative value: end > start");
    }

    /**
     * method for processing information about whether the task is repeated
     *
     * @return repeat a task
     */
    public boolean isRepeated() {
        isRepeated = repeated > 0;
        return isRepeated;
    }

    /**
     * method returns information about this task
     *
     * @return message about task
     */
    public String toString() {
        if ((active) && (!isRepeated)) { //for active single task
            return "Task " + getTitle() + " at " + getTime();
        } else if (active) { //for for active repeated task
            return "Task " + getTitle() + " from " + getStartTime() + " to " + getEndTime() + " every " + getRepeatInterval() + " seconds";
        } else { //for inactive task
            return "Task " + getTitle() + " is inactive";
        }
    }

    /**
     * method returns the time of the next notification after the specified time
     *
     * @param time notification time
     * @return required value
     */
    public int nextTimeAfter(int time) throws IllegalArgumentException {
        int start = this.start;
        if (time < 0)
            throw new IllegalArgumentException();
        if (!active)
            return -1;
        if (time < this.start) {
            return this.start;
        } else {
            while ((start + this.repeated <= this.end) && (this.repeated != 0)) {
                start += this.repeated;
                if (start > time)
                    return start;
            }
        }
        return -1;
    }


}
