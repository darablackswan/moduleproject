package ua.edu.sumdu.ta.Lebedeva.pr5;

public class LinkedTaskList extends AbstractTaskList {
    /**
     * static class for link's struct
     */
    static class Link {
        public Task task;
        public Link next;

        /**
         * constructor for create link
         */
        public Link() {
        }

        /**
         * method gets task
         *
         * @return task
         */
        public Task getTask() {
            return task;
        }

        /**
         * method gets link on next elements
         *
         * @return next
         */
        public Link getNext() {
            return next;
        }

        /**
         * method sets the task value of the local variable
         *
         * @param task
         */
        public void setTask(Task task) {
            this.task = task;
        }

        /**
         * method sets the next value of the local variable
         *
         * @param next
         */
        public void setNext(Link next) {
            this.next = next;
        }
    }

    /**
     * head is start of list
     * tail is end of list
     */
    private Link head;
    private Link tail;
    public int size = 0;

    /**
     * method gets head
     *
     * @return head
     */
    public Link getHead() {
        return this.head;
    }

    /**
     * method gets tail
     *
     * @return tail
     */
    public Link getTail() {
        return this.tail;
    }

    /**
     * method sets head value
     *
     * @param head
     */
    public void setHead(Link head) {
        this.head = head;
    }

    /**
     * method sets tail value
     *
     * @param tail
     */
    public void setTail(Link tail) {
        this.tail = tail;
    }

    /**
     * method for add new non-unique task
     *
     * @param task
     */
    @Override
    public void add(Task task) throws IllegalArgumentException {
        if (task == null)
            throw new IllegalArgumentException();
        Link list = new Link();
        list.setTask(task);
        if (getTail() == null) {
            head = list;
            tail = list;
        } else {
            tail.next = list;
            tail = list;
        }
        size++;
    }

    /**
     * method for remove all task equals to the input
     *
     * @param task
     */
    @Override
    public void remove(Task task) throws NullPointerException {
        if (task == null)
            throw new NullPointerException();
        if (getHead() == tail) {
            head = null;
            tail = null;
        }
        if (head.task == task) {
            head = head.next;
        }
        Link list = getHead();
        while (list.next != null) {
            if (list.next.task == task) {
                if (list.next == tail)
                    tail = list;
                else list.next = list.next.next;
            }
            list = list.next;
        }
        size--;
//        if (size < -1)
//            size = 0;
    }

    /**
     * method gets the task under the specified index
     *
     * @param index specified index
     * @return task
     */
    @Override
    public Task getTask(int index) throws ArrayIndexOutOfBoundsException {
        if ((index < 0) || (index > size() - 1))
            throw new ArrayIndexOutOfBoundsException();
        Link list = head;
        for (int i = 0; i < index; i++)
            list = list.next;
        return list.task;
    }

    /**
     * method returns list of task, which have notification time between from and to
     *
     * @param from start notification
     * @param to   end notification
     * @return
     */
    @Override
    public Task[] incoming(int from, int to) throws IllegalArgumentException {
        if ((from < 0) || (to < 0) || (to < from))
            throw new IllegalArgumentException();
        int newsize = 0;
        Task[] elements = new Task[size()];
        Link list = head;
        while (list != null) {
            if ((list.task.nextTimeAfter(from) == -1) || (list.task.nextTimeAfter(from) > to))
                list = list.next;
            else {
                elements[newsize] = list.task;
                list = list.next;
                newsize++;
            }
        }
        Task[] additional = new Task[newsize];
        System.arraycopy(elements, 0, additional, 0, newsize);
        return additional;
    }

    /**
     * method returns size of list
     *
     * @return size
     */
    @Override
    public int size() {
        return size;
    }
}